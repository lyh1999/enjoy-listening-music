window.onload = function() {
    var app = new Vue({
        el: '#app',
        data: {
            query: '任贤齐',
            musicList: [],
            musicUrl: '',
            musicCover: '',
            hotComments: [],
            isPlaying: false,
            isShow: false,
            mvUrl: ''
        },
        methods: {
            searchMusic() {
                var that = this
                axios.get('https://autumnfish.cn/search?keywords=' + this.query)
                    .then(function(res) {
                        // console.log(res);
                        that.musicList = res.data.result.songs
                    }, function(err) {
                        console.log(err);
                    })
            },
            playMusic(musicId) {
                // console.log(musicId);
                var that = this
                axios.get("https://autumnfish.cn/song/url?id=" + musicId)
                    .then(function(res) {
                        // console.log(res.data.data[0].url);
                        // console.log(res);
                        that.musicUrl = res.data.data[0].url
                    }, function(err) {
                        console.log(err);
                    })
                    // 歌曲详情获取
                axios.get('https://autumnfish.cn/song/detail?ids=' + musicId)
                    .then(function(res) {
                        that.musicCover = res.data.songs[0].al.picUrl
                            // console.log(res.data.songs[0].al.picUrl);
                    }, function(err) {
                        console.log(err);
                    })
                    // 歌曲评论获取
                axios.get('https://autumnfish.cn/comment/hot?type=0&id=' + musicId)
                    .then(function(res) {
                        that.hotComments = res.data.hotComments
                    }, function(err) {
                        console.log(err);
                    })
            },
            play() {
                this.isPlaying = true
            },
            pause() {
                this.isPlaying = false
            },
            playMv(mvid) {
                var that = this;
                axios.get("https://autumnfish.cn/mv/url?id=" + mvid).then(
                    function(response) {
                        // console.log(response);
                        console.log(response.data.data.url);
                        that.isShow = true;
                        that.mvUrl = response.data.data.url;
                    },
                    function(err) {}
                );
            },
            hide() {
                this.isShow = false
            }
        }
    })
}